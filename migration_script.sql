-- ----------------------------------------------------------------------------
-- MySQL Workbench Migration
-- Migrated Schemata: newbee
-- Source Schemata: newbee
-- Created: Fri Jun 22 15:58:32 2018
-- Workbench Version: 8.0.11
-- ----------------------------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------------------------------------------------------
-- Schema newbee
-- ----------------------------------------------------------------------------
DROP SCHEMA IF EXISTS `newbee` ;
CREATE SCHEMA IF NOT EXISTS `newbee` ;

-- ----------------------------------------------------------------------------
-- Table newbee.feedback
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `newbee`.`feedback` (
  `id` INT(10) UNSIGNED NOT NULL,
  `u_id` INT(10) UNSIGNED NOT NULL,
  `feedback` VARCHAR(45) NOT NULL,
  `creation_timestamp` TIMESTAMP NOT NULL,
  INDEX `review_idx` (`id` ASC) VISIBLE,
  INDEX `user_5_idx` (`u_id` ASC) VISIBLE,
  CONSTRAINT `review`
    FOREIGN KEY (`id`)
    REFERENCES `newbee`.`review` (`id`),
  CONSTRAINT `user_5`
    FOREIGN KEY (`u_id`)
    REFERENCES `newbee`.`user` (`u_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

-- ----------------------------------------------------------------------------
-- Table newbee.product
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `newbee`.`product` (
  `p_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(500) NOT NULL,
  `image_url` VARCHAR(500) NOT NULL,
  `product_url` VARCHAR(500) NOT NULL,
  `faq` VARCHAR(500) NOT NULL,
  `available` TINYINT(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`p_id`),
  UNIQUE INDEX `NAME_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 1011
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

-- ----------------------------------------------------------------------------
-- Table newbee.review
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `newbee`.`review` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `p_id` INT(10) UNSIGNED NOT NULL,
  `u_id` INT(10) UNSIGNED NOT NULL,
  `rating` INT(11) NOT NULL,
  `review` VARCHAR(100) NULL DEFAULT NULL,
  `creation_timestamp` TIMESTAMP NOT NULL,
  `verified` TINYINT(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `product_idx` (`p_id` ASC) VISIBLE,
  INDEX `user_4_idx` (`u_id` ASC) VISIBLE,
  CONSTRAINT `product_1`
    FOREIGN KEY (`p_id`)
    REFERENCES `newbee`.`product` (`p_id`),
  CONSTRAINT `user_4`
    FOREIGN KEY (`u_id`)
    REFERENCES `newbee`.`user` (`u_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 20
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

-- ----------------------------------------------------------------------------
-- Table newbee.role
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `newbee`.`role` (
  `r_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`r_id`),
  UNIQUE INDEX `NAME_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
COMMENT = '		';

-- ----------------------------------------------------------------------------
-- Table newbee.user
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `newbee`.`user` (
  `u_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `mob` VARCHAR(10) NOT NULL,
  `img` VARCHAR(500) NULL DEFAULT NULL,
  `dob` DATE NULL DEFAULT NULL,
  `gender` CHAR(1) NULL DEFAULT NULL,
  `password` VARCHAR(45) NOT NULL,
  `creation_timestamp` TIMESTAMP NOT NULL,
  `active` TINYINT(4) NOT NULL DEFAULT '1',
  `user_email` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`u_id`),
  UNIQUE INDEX `user_email_UNIQUE` (`user_email` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 115
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

-- ----------------------------------------------------------------------------
-- Table newbee.user_role
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `newbee`.`user_role` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `u_id` INT(10) UNSIGNED NOT NULL,
  `r_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `user_idx` (`u_id` ASC) VISIBLE,
  INDEX `role_idx` (`r_id` ASC) VISIBLE,
  CONSTRAINT `role`
    FOREIGN KEY (`r_id`)
    REFERENCES `newbee`.`role` (`R_ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `user_1`
    FOREIGN KEY (`u_id`)
    REFERENCES `newbee`.`user` (`u_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 210
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

-- ----------------------------------------------------------------------------
-- Table newbee.vendor
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `newbee`.`vendor` (
  `u_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `firm_name` VARCHAR(45) NOT NULL,
  `location` VARCHAR(45) NOT NULL,
  `gst_no` VARCHAR(45) NOT NULL,
  `pan_no` VARCHAR(45) NOT NULL,
  `gst_certi` VARCHAR(500) NOT NULL,
  `logo` VARCHAR(500) NULL DEFAULT NULL,
  PRIMARY KEY (`u_id`),
  UNIQUE INDEX `firm_name_UNIQUE` (`firm_name` ASC) VISIBLE,
  CONSTRAINT `user_3`
    FOREIGN KEY (`u_id`)
    REFERENCES `newbee`.`user` (`u_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 111
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

-- ----------------------------------------------------------------------------
-- Table newbee.vendor_product
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `newbee`.`vendor_product` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `creation_timestamp` TIMESTAMP NOT NULL,
  `u_id` INT(10) UNSIGNED NOT NULL,
  `p_id` INT(10) UNSIGNED NOT NULL,
  `available` TINYINT(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  INDEX `user_idx` (`u_id` ASC) VISIBLE,
  INDEX `product_idx` (`p_id` ASC) VISIBLE,
  CONSTRAINT `product`
    FOREIGN KEY (`p_id`)
    REFERENCES `newbee`.`product` (`p_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `user`
    FOREIGN KEY (`u_id`)
    REFERENCES `newbee`.`user` (`u_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 60
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;
SET FOREIGN_KEY_CHECKS = 1;
