use newbee;

alter table user auto_increment=100;
insert into user ( name, mob,img,dob,gender, password,creation_timestamp,user_email) values ('gagan','1212355462', '//www.abc.com', '1998-06-17','m','jgdsjb',now(),'gagan.gaur_cs16@gla.ac.in'); 
insert into user ( name, mob,img,dob,gender, password,creation_timestamp,user_email) values ('kumud','1235462367','//www.abc.com' , '1998-06-17','m','jgdsjb',now(),'kumud.gupta_cs16@gla.ac.in');
insert into user ( name, mob,img,dob,gender, password,creation_timestamp,user_email) values ('sajal','1212355462','//www.xyz.com','1998-06-17','f','jgdfhgdf',now(),'sajal.agarwal_ca16@gla.ac.in'); 
insert into user ( name, mob,img,dob,gender, password,creation_timestamp,user_email) values ('kunal','1212355286','//www.xyz.com','1998-06-17','f','jgdfhgdf',now(),'kunal.agarwal_ca16@gla.ac.in');  
insert into user ( name, mob,img,dob,gender, password,creation_timestamp,user_email) values ('kumar','1249462367','//www.abc.com' , '1998-06-17','m','jgdsjb',now(),'mud.gupta_cs16@gla.ac.in');
insert into user ( name, mob,img,dob,gender, password,creation_timestamp,user_email) values ('sanam','1218955462','//www.xyz.com','1998-06-17','f','jgdfhgdf',now(),'jal.agarwal_ca16@gla.ac.in'); 
insert into user ( name, mob,img,dob,gender, password,creation_timestamp,user_email) values ('ankit','1212378286','//www.xyz.com','1998-06-17','f','jgdfhgdf',now(),'nal.agarwal_ca16@gla.ac.in');  
insert into user ( name, mob,img,dob,gender, password,creation_timestamp,user_email) values ('piyush','8235462367','//www.abc.com' , '1998-06-17','m','jgdsjb',now(),'kumud.gup_cs16@gla.ac.in');
insert into user ( name, mob,img,dob,gender, password,creation_timestamp,user_email) values ('arpit','1214855462','//www.xyz.com','1998-06-17','f','jgdfhgdf',now(),'sajal.agarw_ca16@gla.ac.in'); 
insert into user ( name, mob,img,dob,gender, password,creation_timestamp,user_email) values ('krishna','1212755286','//www.xyz.com','1998-06-17','f','jgdfhgdf',now(),'kunal.agar_ca16@gla.ac.in');  


insert into role values('1','customer');
insert into role values('2','vendor');
insert into role values('3','admin');




insert into user_role values ('200','100','2');
insert into user_role values ('201','101','2');
insert into user_role values ('202','102','2');
insert into user_role values ('203','103','2');
insert into user_role values ('204','104','2');
insert into user_role (u_id,r_id) values ('105','1');

insert into user_role (u_id,r_id) values ('106','1');

insert into user_role (u_id,r_id) values ('107','1');

insert into user_role (u_id,r_id) values ('108','1');



insert into vendor values(100,'VISHAL_MEGA_MART','KRISHNA NAGAR','11AAAAA0000A1A1','ABCDE1231A','HTTP://www.abc.com','www.xyz.com');
insert into vendor values(101,'BIG_BAZAR','TOWNSHIP','11AAAAA0000A1B1','ABCDE1231B','HTTP://www.def.com','www.pqt.com');
insert into vendor values(102,'EASY_DAY','ROOPAM','11AAAAA0000A1C1','ABCDE1231C','HTTP://www.ghi.com','www.uvw.com');
insert into vendor values(103,'SR_DAILY_NEEDS','GOVIND NAGAR','11AAAAA0000A1D1','ABCDE1231D','HTTP://www.jkl.com','www.bdjen.com');
insert into vendor values(104,'PANCHHI STORE','KACHCHI SADAK','11AAAAA0000A1E1','ABCDE1231E','HTTP://www.mno.com','www.kjh.com');




alter table product auto_increment=1000;

insert into product (name,description,image_url,product_url,faq)values("bottles","lorem","www.gmail.com","/bottle.com","dummy data.txt");
  insert into product (name,description,image_url,product_url,faq)values("namkeen" ,"lorem","www.gmail.com","/namkeen.com","dummy data.txt");
   insert into product (name,description,image_url,product_url,faq)values("soap" ,"lorem","www.gmail.com","/soap.com","dummy data.txt");
  insert into product (name,description,image_url,product_url,faq)values("buiscit" ,"lorem","www.gmail.com","/buiscit.com","dummy data.txt");
  insert into product (name,description,image_url,product_url,faq)values("keyboard","lorem","www.gmail.com","/keyboard.com","dummy data.txt");
  insert into product (name,description,image_url,product_url,faq)values("mouse","lorem","www.gmail.com","/mouse.com","dummy data.txt");
  insert into product (name,description,image_url,product_url,faq)values("monitor","lorem","www.gmail.com","/monitor.com","dummy data.txt");
  insert into product (name,description,image_url,product_url,faq)values("mobile","lorem","www.gmail.com","/mobile.com","dummy data.txt");
  insert into product (name,description,image_url,product_url,faq)values("pen","lorem","www.gmail.com","/pen.com","dummy data.txt");
 insert into product (name,description,image_url,product_url,faq)values("register","lorem","www.gmail.com","/register.com","dummy data.txt");

 insert into product (name,description,image_url,product_url,faq)values("laptop","lorem","www.gmail.com","/laptop.com","dummy data.txt");
 



alter table vendor_product auto_increment=50;

insert into  vendor_product (u_id,p_id,creation_timestamp) values ('100','1000',now());
insert into  vendor_product (u_id,p_id,creation_timestamp) values ('100','1005',now());
insert into  vendor_product (u_id,p_id,creation_timestamp) values ('100','1007',now());
insert into  vendor_product (u_id,p_id,creation_timestamp) values ('101','1002',now());
insert into  vendor_product (u_id,p_id,creation_timestamp) values ('101','1005',now());
insert into  vendor_product (u_id,p_id,creation_timestamp) values ('102','1004',now());
insert into  vendor_product (u_id,p_id,creation_timestamp) values ('102','1008',now());
insert into  vendor_product (u_id,p_id,creation_timestamp) values ('103','1010',now());
insert into  vendor_product (u_id,p_id,creation_timestamp) values ('104','1009',now());
insert into  vendor_product (u_id,p_id,creation_timestamp) values ('104','1002',now());




alter table review auto_increment=10;

insert into review(p_id,u_id,rating,review,creation_timestamp)values(1002,102,3,"lorem",now());

insert into review(p_id,u_id,rating,review,creation_timestamp)values(1002,101,2,"lorem",now());

insert into review(p_id,u_id,rating,review,creation_timestamp)values(1004,101,5,"lorem",now());

insert into review(p_id,u_id,rating,review,creation_timestamp)values(1006,100,3,"lorem",now());

insert into review(p_id,u_id,rating,review,creation_timestamp)values(1009,105,2,"lorem",now());

insert into review(p_id,u_id,rating,review,creation_timestamp)values(1006,108,1,"lorem",now());

insert into review(p_id,u_id,rating,review,creation_timestamp)values(1008,109,3,"lorem",now());

insert into review(p_id,u_id,rating,review,creation_timestamp)values(1001,103,5,"lorem",now());

insert into review(p_id,u_id,rating,review,creation_timestamp)values(1002,103,5,"lorem",now());

insert into review(p_id,u_id,rating,review,creation_timestamp)values(1003,104,1,"lorem",now());




insert into feedback (id,u_id,feedback,creation_timestamp) values (10,100,1,now());
insert into feedback (id,u_id,feedback,creation_timestamp) values (10,101,3,now());
insert into feedback (id,u_id,feedback,creation_timestamp) values (11,105,2,now());
insert into feedback (id,u_id,feedback,creation_timestamp) values (11,106,1,now());
insert into feedback (id,u_id,feedback,creation_timestamp) values (12,107,4,now());
insert into feedback (id,u_id,feedback,creation_timestamp) values (13,102,2,now());
insert into feedback (id,u_id,feedback,creation_timestamp) values (14,108,1,now());
insert into feedback (id,u_id,feedback,creation_timestamp) values (14,100,1,now());
insert into feedback (id,u_id,feedback,creation_timestamp) values (15,109,3,now());

